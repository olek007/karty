#include "Talia.h"


void Talia::Tasuj()
{
	srand(time(NULL));
	vector<Karta> potasowane;
	int size = _taliaV.size();
	int j = _taliaV.size();
	int random;
	for (int i = 0; i < size; i++)
	{
		random = rand() % j;
		potasowane.push_back(_taliaV[random]);
		_taliaV.erase(_taliaV.begin() + random);
		j--;
	}
	_taliaV = potasowane;
}

void Talia::Rozdaj()
{
	for (int i = 0; i < 52; i++)
	{
		_taliaV.push_back(Karta(i / 13 + 1, i % 13 + 1));
	}
}

void Talia::Rozdaj(int ileKolorow, int ileTalii)
{
	for (int i = 0; i < 13 * ileKolorow * ileTalii; i++)
	{
		_taliaV.push_back(Karta(i / (13 * ileTalii) + 1, i % 13 + 1));
	}
}

void Talia::PokazTalie()
{
	for (int i = 0; i < _taliaV.size(); i++)
	{
		if (_taliaV[i].GetColor() + 2 <= 4)
		{
			SetColor(Red);
		}
		else
			if (_taliaV[i].GetColor() + 2 >= 5)
			{
				SetColor(Black);
			}

		cout << char(_taliaV[i].GetColor() + 2) << " ";

		if (_taliaV[i].GetValue() == 1)
		{
			cout << 'A' << endl;
		}
		else
			if (_taliaV[i].GetValue() == 11)
			{
				cout << 'J' << endl;
			}
			else
				if (_taliaV[i].GetValue() == 12)
				{
					cout << 'Q' << endl;
				}
				else
					if (_taliaV[i].GetValue() == 13)
					{
						cout << 'K' << endl;
					}
					else
						cout << _taliaV[i].GetValue() << endl;
	}
	
	SetColor(Black);
}