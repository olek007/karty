#include "Setting.h"


void Setting::Init()
{
	SetColor(Black);
	system("cls");
}

void Setting::SetColor(Colors kolor)
{
	SetConsoleTextAttribute(_hCon, kolor);
}

void Setting::Write(char ch, int x, int y)
{
	GotoXY(x, y);
	std::cout << ch;
}

void Setting::Write(int i, int x, int y)
{
	GotoXY(x, y);
	std::cout << i;
}

void Setting::Write(std::string tekst, int x, int y)
{
	GotoXY(x, y);
	setlocale(LOCALE_ALL, "Polish");
	std::cout << tekst;
	setlocale(LOCALE_ALL, "C");
}

void Setting::GotoXY(int x, int y)
{
	COORD c;
	c.X = x;
	c.Y = y;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), c);
}
