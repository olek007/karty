#include "Game.h"


void Game::ShowStacks(Stack *stack, int x, int y)
{
	{
		system("cls");
		for (int i = 0; i < 10; i++)
		{
			Write(i + 1, 8 * i + x + 1, y); //char(65+i)
			stack[i].PokazStosUkryty(8 * i + x, y + 2);
		}
	}
}

void Game::GameLoop(Talia *talia)
{
	do
	{
		ShowStacks(_stosy, 2, 1);

		int from, to, count;
		do
		{
			do
			{
				do
				{
					ShowStacks(_stosy, 2, 1);
					Write("Punkty: ", 35, 22); cout << points;
					Write("Pozosta�ych dobra�: ", 55, 22); cout << talia->_taliaV.size() / 10;
					Write("Sk�d: ", 1, 20);
					cin >> from;
				} while (from < 0 || from > 10);

				ShowStacks(_stosy, 2, 1);
				Write("Punkty: ", 35, 22); cout << points;
				Write("Pozosta�ych dobra�: ", 55, 22); cout << talia->_taliaV.size() / 10;
				Write("Sk�d: ", 1, 20); cout << from;
				Write("Dok�d: ", 1, 21);
				cin >> to;
			} while (to < 0 || to > 10);

			ShowStacks(_stosy, 2, 1);
			Write("Punkty: ", 35, 22); cout << points;
			Write("Pozosta�ych dobra�: ", 55, 22); cout << talia->_taliaV.size() / 10;
			Write("Sk�d: ", 1, 20); cout << from;
			Write("Dok�d: ", 1, 21); cout << to;
			Write("Ile: ", 1, 22);
			cin >> count;
		} while (count < 0); //_stosy[from - 1]._stos.size() < count
		if (from > 0 && to > 0 && count > 0)
		{	
			// jak si� udalo przenie��, to odejmij punkt
			if (_stosy[from - 1].PrzeniesDo(&_stosy[to - 1], count)) 
			{
				points--;
			}
		}
		else
		{
			if (from == 0 && to == 0 && count == 0)
			{
				if (talia->_taliaV.size() > 0)
				{
					//jak poda si� 3 zera, to dobiera karty
					FillStacks(talia, 1, 4, 1);
					FillStacks(talia, 5, 10, 1);
					Write("Dobrano karty.", 1, 23);
				}
				else
				{
					Write("Brak mo�liwych kart do doboru.", 1, 23);
				}
			}
			else
			{
				Write("Podano z�e werto�ci.", 1, 23);
			}
		}
		CheckRows(10);
		_getch();
	} while (true);
}

void Game::CreateStacks(int n)
{
	_stosy = new Stack[n];
}

void Game::FillStacks(Talia *skad, int from, int to, int ilomaKartamiWypelnic)
{
	for (int i = from - 1; i < to; i++)
	{
		_stosy[i].Dobierz(skad, ilomaKartamiWypelnic);
	}
}

void Game::CheckRows(int n)
{
	for (int i = 0; i < n ; i++)
	{
		bool flag = true;
		int iter = 1;
		for (int j = _stosy[i]._stos.size() - 2; j >= 0; j--)
		{
			if (_stosy[i]._stos[j].ukryta == false)
			{
				if (_stosy[i]._stos[j + 1].GetColor() == _stosy[i]._stos[j].GetColor() && flag == true)
				{
					if (_stosy[i]._stos[j + 1].GetValue() + 1 == _stosy[i]._stos[j].GetValue() && flag == true)
					{
						flag = true;
						iter++;
						if (flag == true && iter == 13)
						{
							for (int k = 0; k < 13; k++)
							{
								_stosy[i]._stos.pop_back();
							}
							if (_stosy[i]._stos.size() > 0)
							{
								_stosy[i]._stos[_stosy[i]._stos.size() - 1].ukryta = false;
							}
							points += 100;
							break;
						}
					}
					else
					{
						flag = false;
						break;
					}
				}
				else
				{
					flag = false;
					break;
				}
			}
			else
			{
				break;
			}
		}
	}
}

void Game::Intro()
{
	Write("Pasjans Paj�k", 35, 2);

	Write("Celem gry jest u�o�enie stosu z�o�onego z upo��dkowanych kart", 1, 5);
	Write("tego samego koloru (K,Q,J,10,9,8,7,6,5,4,3,2,A).", 1, 6);

	Write("Karty mo�na uk�ada� na przemian kolorami.", 1, 8);

	Write("Karty mo�na uk�ada� tylko mniejsz� na wi�ksz� (np. 7 na 8).", 1, 10);

	Write("Aby do�o�y� do ka�dego stosu po jednej karcie nale�y w polach", 1, 12);
	Write("\"sk�d, dok�d i ile\" wpisa� warto�� 0", 1, 13);

	Write("Naci�nij dowolny klawisz, aby rozpocz�� gr�...", 1, 15);
	_getch();
}