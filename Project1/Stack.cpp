#include "Stack.h"


void Stack::Dobierz(Talia *skad, int ile)
{
	if (ile > 0)
	{
		for (int i = 0; i < ile; i++)
		{
			_stos.push_back(skad->_taliaV[0]);
			skad->_taliaV.erase(skad->_taliaV.begin());
		}
		_stos[_stos.size() - 1].ukryta = false;
	}
}

bool Stack::PrzeniesDo(Stack *dokad, int ile)
{
	if (((int)_stos.size() - ile) >= 0)
	{
		if (_stos[_stos.size() - ile].ukryta == false)
		{
			if (dokad->_stos.size() > 0)
			{		
				if (dokad->_stos[dokad->_stos.size() - 1].GetValue() - 1 == _stos[_stos.size() - ile].GetValue())
				{
					return SprawdzKolejnosc(dokad, ile);
				}
				else
				{
					Write("Z�y ruch. Karty w niedozwolonej kolejno�ci.", 1, 23);
					return false;
				}
			}
			else
			{
				return SprawdzKolejnosc(dokad, ile);
			}
		}
		else
		{
			Write("Z�y ruch. Nie mo�esz przenosi� zakrytych kart.", 1, 23);
			return false;
		}
	}
	else
	{
		Write("Z�y ruch. Ilo�� przenoszonych kart jest za du�a.", 1, 23);
		return false;
	}
}

void Stack::PokazStos(int x, int y)
{
	if (_stos.size() > 0)
	{
		for (int i = 0; i < _stos.size(); i++)
		{
			if (_stos[i].GetColor() + 2 <= 4)
			{
				SetColor(Red);
			}
			else
			if (_stos[i].GetColor() + 2 >= 5)
			{
				SetColor(Black);
			}
			GotoXY(x, y + i);
			cout << char(_stos[i].GetColor() + 2) << " ";

			if (_stos[i].GetValue() == 1)
			{
				cout << 'A' << endl;
			}
			else
			if (_stos[i].GetValue() == 11)
			{
				cout << 'J' << endl;
			}
			else
			if (_stos[i].GetValue() == 12)
			{
				cout << 'Q' << endl;
			}
			else
			if (_stos[i].GetValue() == 13)
			{
				cout << 'K' << endl;
			}
			else
			{
				cout << _stos[i].GetValue() << endl;
			}
		}
		SetColor(Black);
	}
}

void Stack::PokazStosUkryty(int x, int y)
{
	if (_stos.size() > 0)
	{
		for (int i = 0; i < _stos.size(); i++)
		{
			if (_stos[i].ukryta == true)
			{
				GotoXY(x, y + i);
				cout << char(176) << char(176) << char(176) << char(176) << endl;
			}
			else
			{
				if (_stos[i].GetColor() + 2 <= 4)
				{
					SetColor(Red);
				}
				else
				if (_stos[i].GetColor() + 2 >= 5)
				{
					SetColor(Black);
				}

				GotoXY(x, y + i);
				cout << char(_stos[i].GetColor() + 2) << " ";

				if (_stos[i].GetValue() == 1)
				{
					cout << 'A' << endl;
				}
				else
				if (_stos[i].GetValue() == 11)
				{
					cout << 'J' << endl;
				}
				else
				if (_stos[i].GetValue() == 12)
				{
					cout << 'Q' << endl;
				}
				else
				if (_stos[i].GetValue() == 13)
				{
					cout << 'K' << endl;
				}
				else
				{
					cout << _stos[i].GetValue() << endl;
				}
			}
		}
		SetColor(Black);
	}
}

bool Stack::SprawdzKolejnosc(Stack *dokad, int ile)
{
	bool flag = true;
	if (_stos.size() > 1 && ile > 1)
	{
		for (int i = _stos.size() - 1; i > _stos.size() - ile; i--) // -2
		{
			if (_stos[i].GetColor() == _stos[i-1].GetColor() && flag == true) // +1 -0
			{
				if (_stos[i].GetValue() + 1 == _stos[i-1].GetValue() && flag == true)//+1 -0
				{
					flag = true;
				}
				else
				{
					flag = false;
				}
			}
			else
			{
				flag = false;
			}
		}
	}

	if (flag == true)
	{
		if (_stos.size() > 0 && _stos.size() - ile >= 0)
		{
			for (int j = ile; j > 0; j--)
			{
				dokad->_stos.push_back(_stos[_stos.size() - j]);
				_stos.erase(_stos.end() - j);
			}
			Write("Gotowe", 1, 23);
		}
		if (_stos.size() > 0)
		{
			_stos[_stos.size() - 1].ukryta = false;
		}
		return true;
	}
	else
	{
		Write("Z�y ruch. Karty nie s� po kolei.", 1, 23);
		return false;
	}
}
