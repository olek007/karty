#pragma once

#include "Talia.h"

class Stack : public Talia
{
public:

	vector<Karta> _stos;

	void Dobierz(Talia *skad, int ile);

	bool PrzeniesDo(Stack *dokad, int ile);

	void PokazStos(int x, int y);

	void PokazStosUkryty(int x, int y);

private:

	bool SprawdzKolejnosc(Stack *dokad, int ile);
};