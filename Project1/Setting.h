#pragma once

#include <iostream>
#include <conio.h>
#include <string>
#include <vector>
#include <time.h>
#include <Windows.h>

using namespace std;

class Setting
{
public:

	enum Colors
	{
		Black = (BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY),
		Red = (FOREGROUND_RED | FOREGROUND_INTENSITY | BACKGROUND_INTENSITY | BACKGROUND_BLUE | BACKGROUND_GREEN | BACKGROUND_RED | BACKGROUND_INTENSITY),
	};

	void virtual Init();

	void SetColor(Colors kolor);

	void Write(char ch, int x, int y);

	void Write(int i, int x, int y);

	void Write(std::string tekst, int x, int y);

protected:

	HANDLE _hCon = GetStdHandle(STD_OUTPUT_HANDLE);

	void GotoXY(int x, int y);

};