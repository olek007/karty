#include "Game.h"

int main()
{

	Setting setting;
	setting.Init();

	Game game;

	game.Intro();

	Talia talia;
	talia.Rozdaj(1, 8); // ileKolorow * ileTalii == 8
	talia.Tasuj();

	game.CreateStacks(10);
	game.FillStacks(&talia, 1, 4, 6);
	game.FillStacks(&talia, 5, 10, 5);
	
	game.GameLoop(&talia);

	_getch();
	return 0;
}
