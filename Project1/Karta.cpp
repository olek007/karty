#include "Karta.h"




int Karta::GetIter()
{
	return _iterator;
}

int Karta::GetColor()
{
	return _color;
}

int Karta::GetValue()
{
	return _value;
}

Karta::Karta()
{
	_prev = NULL;
	_head = this;
	_iterator = 0;
	_color = 0;
}

Karta::Karta(Karta *prev)
{
	_prev = prev;
	prev->_next = this;
	prev->_tail = NULL;
	_tail = this;
	_iterator = prev->_iterator + 1;
}

Karta::Karta(int color, int value)
{
	this->_color = color;
	this->_value = value;
	ukryta = true;
}

