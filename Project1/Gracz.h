#pragma once

#include "Talia.h"

class Gracz : public Talia
{
public:

	vector<Karta> _reka;

	void Dobierz(Talia *skad, int ile);

	void Tasuj();

	void PokazReke();

};

