#pragma once

#include "Setting.h"

class Karta : public  Setting
{
private:

	int _iterator;
	int _color;
	int _value;

public:

	Karta *_head;
	Karta *_tail;
	Karta *_next;
	Karta *_prev;

	bool ukryta;

	int GetIter();

	int GetColor();

	int GetValue();

	Karta();

	Karta(Karta *prev);

	Karta(int color, int value);

};