#pragma once

#include "Stack.h"

class Game : public Stack
{
public:

	int points = 500;

	Stack *_stosy;

	void ShowStacks(Stack *stack, int x, int y);

	void GameLoop(Talia *talia);

	void CreateStacks(int n);

	void FillStacks(Talia *skad, int from, int to, int ilomaKartamiWypelnic);

	void CheckRows(int n);

	void Intro();
};

