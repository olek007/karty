#pragma once

#include "Karta.h"

class Talia : public  Karta
{
public:

	vector<Karta> _taliaV;

	void Tasuj();

	void Rozdaj();

	void Rozdaj(int ileKolorow, int ileTalii);

	void PokazTalie();

};