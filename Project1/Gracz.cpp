#include "Gracz.h"


void Gracz::Dobierz(Talia *skad, int ile)
{
	for (int i = 0; i < ile; i++)
	{
		_reka.push_back(skad->_taliaV[0]);
		skad->_taliaV.erase(skad->_taliaV.begin());
	}
}

void Gracz::Tasuj()
{
	srand(time(NULL));
	vector<Karta> potasowane;
	int size = _reka.size();
	int j = _reka.size();
	int random;
	for (int i = 0; i < size; i++)
	{
		random = rand() % j;
		potasowane.push_back(_reka[random]);
		_reka.erase(_reka.begin() + random);
		j--;
	}
	_reka = potasowane;
}

void Gracz::PokazReke()
{
	for (int i = 0; i < _reka.size(); i++)
	{
		if (_reka[i].GetColor() + 2 <= 4)
		{
			SetColor(Red);
		}
		else
			if (_reka[i].GetColor() + 2 >= 5)
			{
				SetColor(Black);
			}

		cout << char(_reka[i].GetColor() + 2) << " ";

		if (_reka[i].GetValue() == 1)
		{
			cout << 'A' << endl;
		}
		else
			if (_reka[i].GetValue() == 11)
			{
				cout << 'J' << endl;
			}
			else
				if (_reka[i].GetValue() == 12)
				{
					cout << 'Q' << endl;
				}
				else
					if (_reka[i].GetValue() == 13)
					{
						cout << 'K' << endl;
					}
					else
						cout << _reka[i].GetValue() << endl;
	}
	SetColor(Black);
}
